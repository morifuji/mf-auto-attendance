const puppeteer = require('puppeteer');
const config = require('./config.js')

console.log("action: " + process.argv[2])

let isInAttendance  = null
switch(process.argv[2]) {
  case "in": 
    isInAttendance = true
    break;
  case "out":
    isInAttendance = false
    break;
}

if (null === isInAttendance) {
  console.error("引数に `in` または `out` を設定してください")
  return;
}

if (!config.id || !config.pass) {
  console.error("config.jsでID/パスワードを設定してください")
  return;
}

(async () => {
  const browser = await puppeteer.launch({
    args: ['--no-sandbox', '--disable-setuid-sandbox']
  });
  const page = await browser.newPage();
  await page.goto('https://attendance.moneyforward.com/employee_session/new', {waitUntil: 'networkidle2'});

  await page.type('input[id=employee_session_form_office_account_name]', config.corporation);
  await page.type('input[id=employee_session_form_account_name_or_email]', config.id);
  await page.type('input[id=employee_session_form_password]', config.pass);

  const inputElement = await page.$('input[name=commit]');
  const [response] = await Promise.all([
    page.waitForNavigation(),
    inputElement.click(),
  ]);

  // 出勤ボタンを押す
  let attendanceButton = null
  if (isInAttendance) {
    attendanceButton = await page.$('.attendance-card-time-stamp-icon.attendance-card-time-stamp-clock-in');
  } else {
    attendanceButton = await page.$('.attendance-card-time-stamp-icon.attendance-card-time-stamp-clock-out');
  }

  await attendanceButton.click();

  // 5秒待つ
  await page.waitForSelector('.attendance-notification.is-success', {
    timeout: 5000
  })
  await browser.close();
})();