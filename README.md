# 概要

１コマンドで●Fクラウド給与の出勤・退勤ができるスクリプト。

# 環境構築

1. ライブラリインストール
2. MonerForwardクラウド給与 ~~●Fクラウド給与~~ の認証情報を記載

```
#1 ライブラリインストール
$ yarn
# または
$ npm install

#2 MonerForwardクラウド給与 ~~●Fクラウド給与~~ の認証情報を記載
$ cp sample.config.js config.js
$ vi confing.js

```

# 利用方法

## 出勤

```
yarn mf-in
```

## 退勤

```
yarn mf-out
```

# エイリアス設定

## mac

```
echo -e "alias mf-in='cd ~/mf/ && yarn mf-in'\nalias mf-out='cd ~/mf/ && yarn mf-out'" >> ~/.bashrc
```

# Docker化

**現在調整中**

Dockerfileにして、環境依存なしにしたい

```
docker run mf-in
docker run mf-out
```
